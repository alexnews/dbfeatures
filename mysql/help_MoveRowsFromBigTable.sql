-- Alex Kargin bitbucket.org/alexnews/dbfeatures/
-- In one of the task I have to move table with 10mln records with less distraction, because it is the main table for the Users. But how you can do it? Yes, idea is move with less portions of records, so I come up with easy script, it is took me ~2 hours to write and debug. So here is the code:

DROP PROCEDURE IF EXISTS `help_MoveRowsFromBigTable`;



DELIMITER ;;
CREATE PROCEDURE `help_MoveRowsFromBigTable`(
IN TableName VARCHAR(200),
IN NewTableName VARCHAR(200),
IN HowManyPerMove INT(10)
)

BEGIN

	SET @sql_txt = CONCAT('DROP TABLE IF EXISTS ',NewTableName);
	PREPARE sql_exec FROM @sql_txt;
	EXECUTE sql_exec;
	DEALLOCATE PREPARE sql_exec;

	SET @sql_txt = concat('CREATE TABLE ',NewTableName,' LIKE ',TableName);
	PREPARE sql_exec FROM @sql_txt;
	EXECUTE sql_exec;
	DEALLOCATE PREPARE sql_exec;

	SET @sql_txt = concat('SELECT count(*) INTO @total FROM ',TableName);
	PREPARE sql_exec FROM @sql_txt;
	EXECUTE sql_exec;
	DEALLOCATE PREPARE sql_exec;

	SET @position := 0;
	label1: WHILE @position < @total DO
		SET @sql_txt = concat('INSERT IGNORE INTO ',NewTableName,' SELECT * FROM ',TableName,' LIMIT ',@position,', ',HowManyPerMove);
		PREPARE sql_exec FROM @sql_txt;
		START TRANSACTION;
		EXECUTE sql_exec;
		COMMIT;
		DEALLOCATE PREPARE sql_exec;
		SET @position := @position + HowManyPerMove;
		IF @position > @tolal THEN
			SET @position := @total;
		END IF;
	END WHILE label1;

	SET @sql_txt = concat('SELECT count(*) INTO @totalNew FROM ',NewTableName);
	PREPARE sql_exec FROM @sql_txt;
	EXECUTE sql_exec;
	DEALLOCATE PREPARE sql_exec;

	SELECT CONCAT('Old Table ',TableName,': ',@total,' - New Table',NewTableName,': ',@totalNew);

END;;
DELIMITER ;

CALL help_MoveRowsFromBigTable('Users','Users_new',10000);