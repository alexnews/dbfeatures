-- Alex Kargin bitbucket.org/alexnews/dbfeatures/
-- Check Unindexed tables

DROP PROCEDURE IF EXISTS `system_CheckIndex`;

DELIMITER ;;
CREATE PROCEDURE `system_CheckIndex`(
IN pIndex VARCHAR(100)
)

BEGIN

SELECT t.TABLE_SCHEMA
     , t.TABLE_NAME
     , c.COLUMN_NAME
     , IFNULL(kcu.CONSTRAINT_NAME, 'Not indexed') AS Indexed
FROM information_schema.TABLES as t
         INNER JOIN information_schema.COLUMNS as c
                    ON c.TABLE_SCHEMA = t.TABLE_SCHEMA
                        AND c.TABLE_NAME = t.TABLE_NAME
                        AND c.COLUMN_NAME LIKE '%pIndex%'
         LEFT JOIN information_schema.KEY_COLUMN_USAGE as kcu
                   ON kcu.TABLE_SCHEMA = t.TABLE_SCHEMA
                       AND kcu.TABLE_NAME = t.TABLE_NAME
                       AND kcu.COLUMN_NAME = c.COLUMN_NAME
                       AND kcu.ORDINAL_POSITION = 1
WHERE kcu.TABLE_SCHEMA IS NULL
  AND t.TABLE_SCHEMA NOT IN ('information_schema', 'performance_schema', 'mysql');

END;;
DELIMITER ;

CALL system_CheckIndex('Id');